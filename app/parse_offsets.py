import json
import logging
import argparse
import sys

logger = logging.getLogger("svganim.fixer")
argParser = argparse.ArgumentParser(
    description="Helper tool to fix timings after glitch"
)
argParser.add_argument(
    "--input", type=str, help="Filename to run on (.json_appendable)"
)
args = argParser.parse_args()


with open(args.input, "r") as fp:
    events = json.loads("[" + fp.read() + "]")
    time_offset = 0
    for i, event in enumerate(events):
        if type(event) is list:
            pass
        elif event['event'] == "offset":
            time_offset += event['offset']
            # print a reverseable event that is ignored by this script
            event = {
                'event': 'added_offset',
                'offset': event['offset']
            }
        elif event['event'] == 'added_offset':
            logger.warning('ignore existing offset')
            continue

        elif event["event"] == "viewbox":
            for key, box in enumerate(event['viewboxes']):
                event['viewboxes'][key]['t'] += time_offset
        elif event["event"] == "stroke":
            event['points'] = [[p[0], p[1], p[2], p[3] + time_offset]
                               for p in event['points']]

        if i>0:
            sys.stdout.write(",\n")    
        sys.stdout.write(json.dumps(event))

if time_offset == 0:
    logger.warning('\n\nNo offset defined in file, please do so by adding {"event":"offset", "offset": miliseconds} at the line from which the offset should be added.')