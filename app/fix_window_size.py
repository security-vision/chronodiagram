"""
Some recordings start with the wrong window size, probably because they were started on the laptop screen. 
This script modifies the opening dimensions and all subsequent viewbox elements to adhere to the new size.
"""

import json
import logging
import argparse
import sys

# TODO the whole script (still a copy)

logger = logging.getLogger("svganim.fixer")
argParser = argparse.ArgumentParser(
    description="""Helper tool to fix the window size when the recording started with a smaller screen that full screen.

    Beware, when used with a shell stdin redirect, do not point it towards the input file. That file will be truncated before reading!
    """
)
argParser.add_argument(
    "--input", type=str, help="Filename to run on (.json_appendable)"
)
argParser.add_argument(
    "--width", type=int, help="New width in px"
)
argParser.add_argument(
    "--height", type=int, help="New height in px"
)
args = argParser.parse_args()


with open(args.input, "r") as fp:
    events = json.loads("[" + fp.read() + "]")
    extrabox = None
    for i, event in enumerate(events):
        if type(event) is list:
            # if event[1] != args.width or event[2] != args.height:
            #     # correct default position
            #     # extrabox = {"event": "viewbox", "viewboxes": [{
            #     #     't': 0,
            #     #     'x':  (event[1] - args.width)/2,
            #     #     'y':  (event[2] - args.height)/2,
            #     #     'width': args.width,
            #     #     'height': args.height,
            #     # }], 'original_size': [event[1], event[2]]}
            event[1] = args.width
            event[2] = args.height
        elif event["event"] == "viewbox":
            for key, box in enumerate(event['viewboxes']):
                # {"t": 1088912, "x": 0, "y": 0, "width": 2048, "height": 1152},
                box['x'] += (box['width'] - args.width)/2
                box['y'] += (box['height'] - args.height)/2
                box['width'] = args.width
                box['height'] = args.height
                event['viewboxes'][key] = box
        # elif event["event"] == "stroke":
        #     event['points'] = [[p[0], p[1], p[2], p[3] + time_offset]
        #                        for p in event['points']]

        if i>0:
            sys.stdout.write(",\n")    
        sys.stdout.write(json.dumps(event))
        if extrabox:
            sys.stdout.write(",\n")
            sys.stdout.write(json.dumps(extrabox))
            extrabox = None


