class Playlist {
    constructor(wrapperEl, url) {
        this.wrapperEl = wrapperEl;

        this.onlyWithTitle = true;

        const request = new Request(url, {
            method: 'GET',
        });

        fetch(request)
            .then(response => response.json())
            .then(data => {
                this.files = data;
                this.buildList()
            });

    }

    buildList() {
        let playlist = this.wrapperEl.querySelector('.playlist');
        if (!playlist) {
            playlist = document.createElement('nav');
            playlist.classList.add('playlist');
            this.wrapperEl.appendChild(playlist)
        }
        else {
            playlist.innerHTML = "";
        }

        const filterEl = document.createElement('label');
        filterEl.classList.add('filter');
        filterEl.innerText = "Show only diagrams with title"
        const filterCheckEl = document.createElement('input');
        filterCheckEl.type = "checkbox";
        filterCheckEl.checked = this.onlyWithTitle;
        filterCheckEl.addEventListener('click', (ev) => {
            this.onlyWithTitle = ev.target.checked;
            this.buildList()
        })
        filterEl.appendChild(filterCheckEl)

        playlist.appendChild(filterEl)

        const listEl = document.createElement("ul");
        for (let file of this.files) {

            if(this.onlyWithTitle && file.title === null) { 
                continue;
            }
            const liEl = document.createElement("li");

            const imgEl = document.createElement("img");
            imgEl.classList.add('img');
            imgEl.title = file.id;
            imgEl.src = file.svg;
            liEl.append(imgEl);

            if (file.title) {
                const titleEl = document.createElement("span");
                titleEl.classList.add('title');
                titleEl.innerText = file.title;
                liEl.append(titleEl);
            }


            let time = file.mtime;
            if (file.ctime != file.mtime) {
                time += ` (orig: ${file.ctime})`;
            }
            const dateEl = document.createElement("span");
            dateEl.classList.add('date');
            dateEl.innerText = time;
            liEl.append(dateEl);


            const nameEl = document.createElement("span");
            nameEl.classList.add('name');
            nameEl.innerText = file.name;
            liEl.append(nameEl);

            const linksEl = document.createElement("span");
            linksEl.classList.add('links');
            liEl.append(linksEl);

            const playEl = document.createElement("a");
            playEl.classList.add('play');
            playEl.innerText = "Play";
            playEl.href = location;
            playEl.pathname = "annotate.html";
            playEl.search = "?file=" + file.name + "&player=1";
            linksEl.append(playEl);

            const annotateEl = document.createElement("a");
            annotateEl.classList.add('annotate');
            annotateEl.innerText = "Annotate";
            annotateEl.href = location;
            annotateEl.pathname = "annotate.html";
            annotateEl.search = "?file=" + file.name;
            linksEl.append(annotateEl);

            const drawEl = document.createElement("a");
            drawEl.classList.add('draw');
            drawEl.innerText = "Draw";
            drawEl.href = location;
            drawEl.pathname = "draw.html";
            drawEl.hash = file.id;
            linksEl.append(drawEl);

            // liEl.addEventListener('click', (e) => {
            //     this.play(fileUrl);
            //     playlist.style.display = "none";
            // });
            listEl.appendChild(liEl);
        }
        playlist.appendChild(listEl);
        // do something with the data sent in the request
    }
}