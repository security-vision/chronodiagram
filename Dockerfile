FROM python:3.9

# TODO manually compile ffmpeg, to prevent useless deps and decrease build size
# for that, see here: https://stackoverflow.com/questions/53944487/how-to-install-ffmpeg-in-a-docker-container
# pin ffmpeg version to prevent issues down the road
RUN apt-get update && apt-get -y install ffmpeg=7:4.3.*

RUN mkdir /app 
COPY pyproject.toml /app 
COPY poetry.lock /app 

WORKDIR /app
ENV PYTHONPATH=${PYTHONPATH}:${PWD} 
RUN pip3 install poetry
RUN poetry config virtualenvs.create false
RUN poetry install --no-dev

# copy files as late as possible, these change the most
# forcing a rerun of all layers
COPY /app /app


ENTRYPOINT poetry run python webserver.py --storage /files
